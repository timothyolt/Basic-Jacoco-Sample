import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MainTest {
    @Test
    void test1() {
        Assertions.assertEquals(-794,
                Main.computeTax(
                        new double[] { 125000 },
                        new int[] { 9, 8, 7, 5 },
                        new int[] { 40 }
                )
        );
    }
}
